<!-- header section start -->
		<header>
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="left floatleft">
								<ul>
									<li>
										<i class="fa fa-phone"></i> 
										+11 (019) 25184203
									</li>
									<li>
										<i class="fa fa-envelope-o"></i> 
										info@glidercycles.com
									</li>
								</ul>
							</div>
							<div class="right floatright">
								<ul>
									<li>
										<form action="#">
											<button type="submit">
												<i class="fa fa-search"></i>
											</button>
											<input type="search" placeholder="Search" />
										</form>
									</li>
									<li>
										<i class="fa fa-user"></i> 
										<a href="my-account.html">Account</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sticky-menu" class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 header-bottom-bg">
							<div class="logo floatleft">
								<a href="index.php">
									<img src="img/glidernew.png" alt="Glider" />
								</a>
							</div>
							<div class="mainmenu text-center floatleft">
								<nav>
									<ul>
										<li><a href="index.php">Home</a>
											<!--<ul>
												<li>
													<a href="index.html">Home Version One</a>
												</li>
												<li>
													<a href="index-2.html">Home Version Two</a>
												</li>
												<li>
													<a href="index-3.html">Home Version Three</a>
												</li>
											</ul>-->
										</li>
										<li>
											<a href="#">products</a>
											<!--<ul class="mega-menu">
												<li><h5>men’s bikes</h5>
													<ul>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> T Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> M Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> T Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> L Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> T Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> T Mounten bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> T Mounten bike</a>
														</li>
													</ul>
												</li>
												<li><h5>Accessories</h5>
													<ul>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Giro Cipher Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mountain Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mountain Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Giro Cipher Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Giro Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Giro Helmet</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mountain Helmet</a>
														</li>
													</ul>
												</li>
												<li><h5>women’s bikes</h5>
													<ul>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-arrow-circle-o-up"></i> Mount POW bike</a>
														</li>
													</ul>
												</li>
												<li>
													<div class="banner-hover">
														<a href="#"><img alt="Banner" src="img/banner.jpg"></a>
													</div>
												</li>
											</ul>-->
										</li>
										<li><a href="aboutus.php">about</a></li>
										<!--<li><a href="#">shortcode</a>
											<ul>
												<li>
													<a href="shortcode/alerts.html">alerts</a>
												</li>
												<li>
													<a href="shortcode/buttons.html">buttons</a>
												</li>
												<li>
													<a href="shortcode/blog.html">blog</a>
												</li>
												<li>
													<a href="shortcode/collapse.html">collapse</a>
												</li>
												<li>
													<a href="shortcode/service.html">service</a>
												</li>
												<li>
													<a href="shortcode/form.html">form</a>
												</li>
												<li>
													<a href="shortcode/map.html">map</a>
												</li>
												<li>
													<a href="shortcode/tab.html">tab</a>
												</li>
												<li>
													<a href="shortcode/team.html">team</a>
												</li>
												<li>
													<a href="shortcode/product-slider.html">product slider</a>
												</li>
												<li>
													<a href="shortcode/video.html">video</a>
												</li>
											</ul>
										</li>-->
										<!--<li><a href="blog-1.html">blog</a>
											<ul>
												<li>
													<a href="blog-1.html">Blog Style One</a>
												</li>
												<li>
													<a href="blog-2.html">Blog Style Two</a>
												</li>
												<li>
													<a href="single-blog.html">Single Blog</a>
												</li>
											</ul>
										</li>-->
										<li><a href="#">Pages</a>
											<!--<ul>
												<li>
													<a href="login.html">Login</a>
												</li>
												<li>
													<a href="my-account.html">My account</a>
												</li>
												<li>
													<a href="checkout.html">CheckOut</a>
												</li>
												<li>
													<a href="wishlist.html">Wishlist</a>
												</li>
												<li>
													<a href="cart.html">Cart</a>
												</li>
												<li>
													<a href="about.html">About</a>
												</li>
												<li>
													<a href="contact.html">Contact</a>
												</li>
												<li>
													<a href="shop.html">Shop</a>
												</li>
												<li>
													<a href="shop-list-left.html">Shop Left List View</a>
												</li>
												<li>
													<a href="shop-list-right.html">Shop Right List View</a>
												</li>
												<li>
													<a href="shop-gird-left.html">Shop Left Gird View</a>
												</li>
												<li>
													<a href="shop-gird-right.html">Shop Right Gird View</a>
												</li>
												<li>
													<a href="product-details.html">Product Details</a>
												</li>
												<li>
													<a href="error-404.html">404 Error</a>
												</li>
											</ul>-->
										</li>
										<li>
											<a href="contact.php">contact</a>
										</li>
									</ul>
								</nav>
							</div>
							<!-- mobile menu start -->
							<div class="mobile-menu-area">
								<div class="mobile-menu">
									<nav id="dropdown">
										<ul>
											<li><a href="index.html">Home</a>
												<!--<ul>
													<li>
														<a href="index.html">Home Version One</a>
													</li>
													<li>
														<a href="index-2.html">Home Version Two</a>
													</li>
													<li>
														<a href="index-3.html">Home Version Three</a>
													</li>
												</ul>-->
											</li>
											<li>
												<a href="#">products</a>
												<ul>
													<li><h5>men’s bikes</h5>
														<ul>
															<li>
																<a href="#">T Mounten bike</a>
															</li>
															<li>
																<a href="#">M Mounten bike</a>
															</li>
															<li>
																<a href="#">T Mounten bike</a>
															</li>
															<li>
																<a href="#">L Mounten bike</a>
															</li>
															<li>
																<a href="#">T Mounten bike</a>
															</li>
															<li>
																<a href="#">T Mounten bike</a>
															</li>
															<li>
																<a href="#">T Mounten bike</a>
															</li>
														</ul>
													</li>
													<li><h5>Accessories</h5>
														<ul>
															<li>
																<a href="#">Giro Cipher Helmet</a>
															</li>
															<li>
																<a href="#">Mountain Helmet</a>
															</li>
															<li>
																<a href="#">Mountain Helmet</a>
															</li>
															<li>
																<a href="#">Giro Cipher Helmet</a>
															</li>
															<li>
																<a href="#">Giro Helmet</a>
															</li>
															<li>
																<a href="#">Giro Helmet</a>
															</li>
															<li>
																<a href="#">Mountain Helmet</a>
															</li>
														</ul>
													</li>
													<li><h5>women’s bikes</h5>
														<ul>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
															<li>
																<a href="#">Mount POW bike</a>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li><a href="aboutus.php">about</a></li>
											<!--<li><a href="blog-1.html">blog</a>
												<ul>
													<li>
														<a href="blog-1.html">Blog Style One</a>
													</li>
													<li>
														<a href="blog-2.html">Blog Style Two</a>
													</li>
													<li>
														<a href="single-blog.html">Single Blog</a>
													</li>
												</ul>
											</li>-->
											<!--<li><a href="#">Pages</a>
												<ul>
													<li>
														<a href="login.html">Login</a>
													</li>
													<li>
														<a href="my-account.html">My account</a>
													</li>
													<li>
														<a href="checkout.html">CheckOut</a>
													</li>
													<li>
														<a href="wishlist.html">Wishlist</a>
													</li>
													<li>
														<a href="cart.html">Cart</a>
													</li>
													<li>
														<a href="about.html">About</a>
													</li>
													<li>
														<a href="contact.html">Contact</a>
													</li>
													<li>
														<a href="shop.html">Shop</a>
													</li>
													<li>
														<a href="shop-list-left.html">Shop Left List View</a>
													</li>
													<li>
														<a href="shop-list-right.html">Shop Right List View</a>
													</li>
													<li>
														<a href="shop-gird-left.html">Shop Left Gird View</a>
													</li>
													<li>
														<a href="shop-gird-right.html">Shop Right Gird View</a>
													</li>
													<li>
														<a href="product-details.html">Product Details</a>
													</li>
													<li>
														<a href="error-404.html">404 Error</a>
													</li>
												</ul>
											</li>-->
											<li>
												<a href="contact.php">contact</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
							<!-- mobile menu end -->
							<div class="cart-menu-area floatright">
								<ul>
									<li><a href="#"><i class="pe-7s-shopbag"></i> <span>2</span></a>
										<ul class="cart-menu">
											<li>
												<a href="#"><img src="img/cart/1.png" alt="" /></a>
												<div class="cart-menu-title">
													<a href="cart.html"><h5>Mount POW C058 FG </h5></a>
													<span>1 x $2500</span>
												</div>
												<span class="cancel-item"><i class="fa fa-close"></i></span>
											</li>
											<li>
												<a href="cart.html"><img src="img/cart/1.png" alt="" /></a>
												<div class="cart-menu-title">
													<a href="cart.html"><h5>Mount POW C058 FG </h5></a>
													<span>1 x $2500</span>
												</div>
												<span class="cancel-item"><i class="fa fa-close"></i></span>
											</li>
											<li class="cart-menu-btn">
												<a href="cart.html">view cart</a>
												<a href="checkout.html">checkout</a>
											</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
        <!-- header section end -->