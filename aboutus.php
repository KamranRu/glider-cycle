<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>About || Glider Cycle</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="img/newfavicon.png">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		<!-- all css here -->
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
		
		<?php include_once "header.php"?>

        <!-- header section end -->
		<!-- page banner area start -->
		<div class="page-banner">
			<img src="img/slider/bg2.jpg" alt="Page Banner" />
		</div>
		<!-- page banner area end -->
		<!-- about us section start -->
		<section class="about-area section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-9 col-md-6">
						<div class="about-img">
							<img src="img/about/1.jpg" alt="" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="about-text">
							<h3>About Us</h3>
							<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. Lorem  Lorem  The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', </p> <br />
							<p> making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years,</p>
							<a class="shop-btn" href="#">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- about us section end -->
		<!-- choose section start -->
		<section class="choose-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-text-center">
						<div class="section-title about-title text-center">
							<h3>Why choose us</h3>
							<div class="shape">
								<img src="img/icon/t-shape.png" alt="Title Shape" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="img/about/bg1.jpg" alt="" />
			<div class="section-padding">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-tools"></i>
								<h3>Strong Service</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-refresh"></i>
								<h3>Money back guarantee</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-help2"></i>
								<h3>Support provide</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-home"></i>
								<h3>Home Service</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-hammer"></i>
								<h3>Accessories available</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-choose">
								<i class="pe-7s-map-2"></i>
								<h3>GPS tracker</h3>
								<p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- choose section end -->
		<!-- our team section start -->
		<div class="our-team section-padding-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-text-center">
						<div class="section-title about-title text-center">
							<h3>Our Team</h3>
							<div class="shape">
								<img src="img/icon/t-shape.png" alt="Title Shape" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="single-member">
							<div class="member-img">
								<img src="img/about/2.jpg" alt="Team Member" />
								<div class="social-share text-center">
									<div class="link-icon">
										<a href="#"><i class="fa fa-link"></i></a>
									</div>
									<ul class="clearfix">
										<li>
											<a href="#"><i class="fa fa-google"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-behance"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<h4>Tom smith</h4>
							<p>Designer</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="single-member">
							<div class="member-img">
								<img src="img/about/3.jpg" alt="Team Member" />
								<div class="social-share text-center">
									<div class="link-icon">
										<a href="#"><i class="fa fa-link"></i></a>
									</div>
									<ul class="clearfix">
										<li>
											<a href="#"><i class="fa fa-google"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-behance"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<h4>Lora Smiytl</h4>
							<p>Designer</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="single-member">
							<div class="member-img">
								<img src="img/about/4.jpg" alt="Team Member" />
								<div class="social-share text-center">
									<div class="link-icon">
										<a href="#"><i class="fa fa-link"></i></a>
									</div>
									<ul class="clearfix">
										<li>
											<a href="#"><i class="fa fa-google"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-behance"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<h4>Semuael Samson</h4>
							<p>Designer</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="single-member">
							<div class="member-img">
								<img src="img/about/5.jpg" alt="Team Member" />
								<div class="social-share text-center">
									<div class="link-icon">
										<a href="#"><i class="fa fa-link"></i></a>
									</div>
									<ul class="clearfix">
										<li>
											<a href="#"><i class="fa fa-google"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-behance"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<h4>Larena Maya</h4>
							<p>Designer</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- our team section end -->
		<!-- footer section start -->

		<?php include_once "footer.php"?>
		
		<!-- footer section end -->
		
		<!-- all js here -->
		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- camera slider JS -->
        <script src="js/camera.min.js"></script>
		<!-- jquery.easing js -->
        <script src="js/jquery.easing.1.3.js"></script>
		<!-- slick slider js -->
        <script src="js/slick.min.js"></script>
		<!-- jquery-ui js -->
        <script src="js/jquery-ui.min.js"></script>
		<!-- magnific-popup js -->
        <script src="js/magnific-popup.min.js"></script>
		<!-- countdown js -->
        <script src="js/countdown.js"></script>
		<!-- meanmenu js -->
        <script src="js/jquery.meanmenu.js"></script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>
		
		<!-- Google Map JS -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Agsvf36du-7l_mp8iu1a-rXoKcWfs2I"> </script>
		<!-- Custom map-script js -->
        <script src="js/map-script.js"></script>
    </body>
</html>
