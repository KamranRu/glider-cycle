<!-- contact area end -->
			<div class="footer-logo-text">
				<div class="container text-center">
					<a href="index.html"><img src="img/gliderfooter1.png" alt="Logo" /></a>
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
					<p> suffered alteration in some form, by injected humour, or randomised</p>
				</div>
			</div>
			<!-- footer top start -->
			<div class="footer-top section-padding">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="s-footer-text">
								<div class="footer-title">
									<h4>Socail Network</h4>
								</div>
								<div class="social-link actions-btn clearfix">
									<ul>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
									</ul>
								</div>
								<h4>Newsletter</h4>
								<form action="mail.php">
									<div class="input-text">
										<input type="text" name="email" placeholder="Email Address" />
									</div>
									<div class="submit-text">
										<input type="submit" name="submit" value="submit" />
									</div>
								</form>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="s-footer-text">
								<div class="footer-title">
									<h4>Contact</h4>
								</div>
								<div class="contact-link">
									<ul>
										<li>
											<span>Email:</span><p>Domain@email.com Name@company.info</p>
										</li>
										<li>
											<span>Phone:</span> <p>+111 (019) 25184203 +111 (018) 50950555</p>
										</li>
										<li>
											<span>Address:</span><p>777/a4 Bonosri Road, Danpura East USA. -25000</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2">
							<div class="s-footer-text">
								<div class="footer-title">
									<h4>INFORMATION</h4>
								</div>
								<div class="footer-menu">
									<ul>
										<li>
											<a href="aboutus.html">About Us</a>
										</li>
										<li>
											<a href="terms-conditions.php">Terms & Conditions</a>
										</li>
										<li>
											<a href="faq.php">FAQ</a>
										</li>
										<li>
											<a href="privacy_policy.php">Privacy Policy</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2 col-md-3">
							<div class="s-footer-text">
								<div class="footer-title">
									<h4>My Account</h4>
								</div>
								<div class="footer-menu">
									<ul>
										<li>
											<a href="#">My Orer</a>
										</li>
										<li>
											<a href="#">My Cart</a>
										</li>
										<li>
											<a href="#">Wishlist</a>
										</li>
										<li>
											<a href="#">Login</a>
										</li>
										<li>
											<a href="#">Register</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			<!-- footer bottom start -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="left floatleft">
								<p>Copyright &copy; 2020 - Powered By Folk Technologies <a href="http://www.thefolkstudios.com">thefolkstudios</a></p>
							</div>
							<div class="right mayment-card floatright">
								<ul>
									<li>
										<a href="#"><img src="img/footer/v1.png" alt="Payment Card" /></a>
									</li>
									<li>
										<a href="#"><img src="img/footer/v2.png" alt="Payment Card" /></a>
									</li>
									<li>
										<a href="#"><img src="img/footer/v3.png" alt="Payment Card" /></a>
									</li>
									<li>
										<a href="#"><img src="img/footer/v4.png" alt="Payment Card" /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer bottom end -->
		</footer>
		<!-- footer section end -->