<!doctype html>
<html class="no-js" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Contact || Glider Cycle</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- favicon -->
      <link rel="shortcut icon" type="image/x-icon" href="img/newfavicon.png">
      <link rel="apple-touch-icon" href="apple-touch-icon.png">
      <!-- Place favicon.ico in the root directory -->
      <!-- all css here -->
      <!-- style css -->
      <link rel="stylesheet" href="style.css">
      <!-- modernizr js -->
      <script src="js/vendor/modernizr-2.8.3.min.js"></script>
   </head>
   <body style="background-color: #f2f3f7;">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <!-- header section start -->
      <?php include_once "header.php"?>
      <!-- header section end -->
      <!-- page banner area start -->
      <div class="page-banner">
         <img src="img/slider/bg2.jpg" alt="Page Banner" />
      </div>
      <!-- page banner area end -->
      <div class="body-content mt-5">
         <div class="container">
            <div class="terms-conditions-page">
               <div class="row">
                  <div class="col-md-12 terms-conditions">
                     <h2 class="heading-title">Terms and Conditions</h2>
                     <div class="">
                        <h3>Intellectual Propertly</h3>
                        <ol>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. </li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                        </ol>
                        <h3>Termination</h3>
                        <ol>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. </li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                           <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                        </ol>
                        <h3>Changes to this agreement</h3>
                        <p>We reserve the right, at our sole discretion, to modify or replace these Terms and Conditions by posting the updated terms on the Site. Your continued use of the Site after any such changes constitutes your acceptance of the new Terms and Conditions. </p>
                        <h3>Contact Us</h3>
                        <p>If you have any questions about this Agreement, please contact us filling this <a href="#" class='contact-form'>contact form</a></p>
                     </div>
                  </div>
               </div>
               <!-- /.row -->
            </div>
            <!-- /.sigin-in-->	
         </div>
         <!-- /.container -->
      </div>
      <!-- /.body-content -->
      <!-- footer section start -->
		
			<?php include_once "footer.php"?>

      <!-- footer section end -->
      <!-- all js here -->
      <!-- jquery latest version -->
      <script src="js/vendor/jquery-1.12.3.min.js"></script>
      <!-- bootstrap js -->
      <script src="js/bootstrap.min.js"></script>
      <!-- camera slider JS -->
      <script src="js/camera.min.js"></script>
      <!-- jquery.easing js -->
      <script src="js/jquery.easing.1.3.js"></script>
      <!-- slick slider js -->
      <script src="js/slick.min.js"></script>
      <!-- jquery-ui js -->
      <script src="js/jquery-ui.min.js"></script>
      <!-- magnific-popup js -->
      <script src="js/magnific-popup.min.js"></script>
      <!-- countdown js -->
      <script src="js/countdown.js"></script>
      <!-- meanmenu js -->
      <script src="js/jquery.meanmenu.js"></script>
      <!-- plugins js -->
      <script src="js/plugins.js"></script>
      <!-- main js -->
      <script src="js/main.js"></script>
      <!-- Google Map JS -->
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Agsvf36du-7l_mp8iu1a-rXoKcWfs2I"> </script>
      <!-- Custom map-script js -->
      <script src="js/map-script.js"></script>
   </body>
</html>