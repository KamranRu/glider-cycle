<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Contact || Glider Cycle</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="img/newfavicon.png">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		<!-- all css here -->
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->

		<?php include_once "header.php"?>

        <!-- header section end -->
		<!-- page banner area start -->
		<div class="page-banner">
			<img src="img/slider/bg2.jpg" alt="Page Banner" />
		</div>
		<!-- page banner area end -->
		<!-- contact area start -->
		<div class="map-contact clearfix">
			<div class="googleMap-info">
				<div class="map-2" id="googleMap"></div>
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="main-contact-info">
								<ul class="contact-info">
									<li><h3>contact info</h3></li>
									<li>
										<i class="fa fa-map-marker"></i>
										<div class="text">
											<p>777 Seventh Road. Bonosri</p>
											<p>Rampura - Dhaka.</p>
										</div>
									</li>
									<li>
										<i class="fa fa-phone"></i>
										<div class="text">
											<p>+11 (019) 25184203</p>
											<p>+11 (019) 25184203</p>
										</div>
									</li>
									<li>
										<i class="fa fa-paper-plane-o"></i>
										<div class="text">
											<p>domain@email.com</p>
											<p>mail@email.com</p>
										</div>
									</li>
									<li>
										<div class="social-share contact-social">
											<ul class="clearfix">
												<li>
													<a href="#"><i class="fa fa-google"></i></a>
												</li>
												<li>
													<a href="#"><i class="fa fa-facebook"></i></a>
												</li>
												<li>
													<a href="#"><i class="fa fa-twitter"></i></a>
												</li>
												<li>
													<a href="#"><i class="fa fa-behance"></i></a>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- contact-informaion start -->
		<div class="contact-informaion section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="informaion-text">
							<h3>Information</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
							<p> Printing and typesetting industry. Lorem Ipsum has been the industry's dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row">
							<div class="contact-form clearfix">
								<form action="mail.php" method="post">
									<div class="col-sm-6">
										<div class="input-text">
											<input type="text" name="name" placeholder="Your Name" />
										</div>
									</div>
									<div class="col-sm-6">
										<div class="input-text">
											<input type="text" name="email" placeholder="Email" required />
										</div>
									</div>
									<div class="col-xs-12">
										<div class="input-text">
											<input type="text" name="subject" placeholder="Subject" />
										</div>
									</div>
									<div class="col-xs-12">
										<div class="input-text">
											<textarea name="message" placeholder="Message" rows="4"></textarea>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="submit-text">
											<input type="submit" name="submit" value="submit" />
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- contact-informaion end -->
		<!-- footer section start -->

		<?php include_once "footer.php"?>
		
		<!-- footer section end -->
		
		<!-- all js here -->
		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- camera slider JS -->
        <script src="js/camera.min.js"></script>
		<!-- jquery.easing js -->
        <script src="js/jquery.easing.1.3.js"></script>
		<!-- slick slider js -->
        <script src="js/slick.min.js"></script>
		<!-- jquery-ui js -->
        <script src="js/jquery-ui.min.js"></script>
		<!-- magnific-popup js -->
        <script src="js/magnific-popup.min.js"></script>
		<!-- countdown js -->
        <script src="js/countdown.js"></script>
		<!-- meanmenu js -->
        <script src="js/jquery.meanmenu.js"></script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>
		
		<!-- Google Map JS -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Agsvf36du-7l_mp8iu1a-rXoKcWfs2I"> </script>
		<!-- Custom map-script js -->
        <script src="js/map-script.js"></script>
    </body>
</html>
