<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Contact || Glider Cycle</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="img/newfavicon.png">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		<!-- all css here -->
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->

		<?php include_once "header.php"?>

        <!-- header section end -->
		<!-- page banner area start -->
		<div class="page-banner">
			<img src="img/slider/bg2.jpg" alt="Page Banner" />
		</div>
		<!-- page banner area end -->
        
        <!-- partial:index.partial.html -->
        <div class="faq-title">
            <h1>FAQ</h1>
        </div>

<div class="accordion">
  <div class="accordion-item">
    <div class="accordion-item-header">
      Where do you deliver within odisha?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
       Currently we deliver to selected cities within Odisha. Please check if we deliver to your pin code/city by entering your pin code details.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <div class="accordion-item-header">
      How soon will I receive my order?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
        The time taken for delivery tends to vary according to the location. we make our best efforts to deliver your order after it is placed.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <div class="accordion-item-header">
      Do I need to register to be able to shop at www.glidercycles.in. ?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
       We recommend that you to register on www.glidercycles.in as it provides multiple benefits i.e; faster checkouts, allows to track orders, add products to your wishlist. 
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <div class="accordion-item-header">
      I don’t remember my password. What do I do?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
        No need to worry, Please >click on Forgot Password link and enter your e-mail address. We will e-mail you a system generated change password link to your registered e-mail address.  
      </div>
    </div>
  </div>
  
 <div class="accordion-item">
    <div class="accordion-item-header">
      Can I cancel my order?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
       Yes, you can cancel your order within 24hrs after your order is placed.  
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <div class="accordion-item-header">
      Are the details that I have shared is secure?
    </div>
    <div class="accordion-item-body">
      <div class="accordion-item-body-content">
       We understand that your privacy is important to you. The details you shared with us we ensure you that we will maintain complete confidentiality . You can refer to our Privacy Policy for any further information.
      </div>
    </div>
  </div>
</div>
<!-- partial -->

		<!-- footer section start -->

		<?php include_once "footer.php"?>
		
		<!-- footer section end -->
		
		<!-- all js here -->
		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- camera slider JS -->
        <script src="js/camera.min.js"></script>
		<!-- jquery.easing js -->
        <script src="js/jquery.easing.1.3.js"></script>
		<!-- slick slider js -->
        <script src="js/slick.min.js"></script>
		<!-- jquery-ui js -->
        <script src="js/jquery-ui.min.js"></script>
		<!-- magnific-popup js -->
        <script src="js/magnific-popup.min.js"></script>
		<!-- countdown js -->
        <script src="js/countdown.js"></script>
		<!-- meanmenu js -->
        <script src="js/jquery.meanmenu.js"></script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>

        <script>
            const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

accordionItemHeaders.forEach(accordionItemHeader => {
  accordionItemHeader.addEventListener("click", event => {
    
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!
    
    // const currentlyActiveAccordionItemHeader = document.querySelector(".accordion-item-header.active");
    // if(currentlyActiveAccordionItemHeader && currentlyActiveAccordionItemHeader!==accordionItemHeader) {
    //   currentlyActiveAccordionItemHeader.classList.toggle("active");
    //   currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    // }

    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if(accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    }
    else {
      accordionItemBody.style.maxHeight = 0;
    }
    
  });
});
        </script>
		
		<!-- Google Map JS -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Agsvf36du-7l_mp8iu1a-rXoKcWfs2I"> </script>
		<!-- Custom map-script js -->
        <script src="js/map-script.js"></script>
    </body>
</html>
